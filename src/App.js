import React from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Live from "./Assets/Components/Pertandingan/live";
import Index from "./Assets/Layouts/Index";
import Login from "./Assets/Pages/Login";
import Register from "./Assets/Pages/Register";
import "./index.css";

function App() {
  return (
    <div>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Index />} />
          <Route path="/login" element={<Login />} />
          <Route path="/register" element={<Register />} />
          <Route path="/live" element={<Live />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
