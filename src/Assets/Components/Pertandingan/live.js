import React from "react";
import Footer from "../../Layouts/Footer";
import Navbar from "../../Layouts/Navbar";
import Header from "../Home/Header";
import Berita from "../Konten/Berita";
import Kategori from "../Konten/Kategori";
import Pertandingan from "../Konten/Pertandingan";
import LogoUEA from "../../Images/uea-logo.png";
import LogoARG from "../../Images/arg-logo.png";
import cuplikan1 from "../../Images/cuplikan1.jpeg";
import cuplikan2 from "../../Images/cuplikan2.jpeg";
import cuplikan3 from "../../Images/cuplikan3.jpeg";

const Live = () => {
  return (
    <div>
      <Navbar />
      <div className="px-60">
        <div className="container">
          <Header />
          <div className="mt-16">
            <div className="flex flex-wrap justify-between">
              <div className="w-full md:w-1/4 border">
                <Kategori />
              </div>
              <div className="w-full md:w-1/2 px-5">
                <div className="border">
                  <h1 className="text-lg font-bold text-center py-2">Pertandingan</h1>
                  <hr />
                  <div className="mt-3 w-full">
                    <h1 className="text-2xl font-sans font-bold text-center">Live Score</h1>
                    <div className="flex justify-center items-center my-5">
                      <div className="flex justify-around items-center w-[150px]">
                        <div className="flex flex-col">
                          <p className="text-sm font-mono mb-3">Argentina</p>
                          <img src={LogoARG} alt="" width={30} className="mx-auto" />
                        </div>
                        <h1 className="text-4xl font-mono">0</h1>
                      </div>
                      <div className="text-4xl">:</div>
                      <div className="flex justify-around items-center w-[150px]">
                        <h1 className="text-4xl font-mono">0</h1>
                        <div className="flex flex-col">
                          <p className="text-sm font-mono mb-3">Uni Emirate</p>
                          <img src={LogoUEA} alt="" width={30} className="mx-auto" />
                        </div>
                      </div>
                    </div>
                    <div className="flex">
                      <h1 className="text-xl font-bold px-5">Statistik Pertandingan :</h1>
                    </div>
                    <div className="px-10 py-3 w-full">
                      <ul className="w-full">
                        <li className="flex text-xs font-mono justify-between">
                          <span>Joni kartu kuning -Argentina</span>
                          <span>'20</span>
                        </li>
                        <li className="flex text-xs font-mono justify-between my-3">
                          <span>Jamal offside -UEA</span> <span>'35</span>
                        </li>
                        <li className="flex text-xs font-mono justify-between">
                          <span>Doni offside -UEA</span> <span>'40</span>
                        </li>
                      </ul>
                    </div>
                    <div className="px-5">
                      <h1 className="text-xl font-bold">Cuplikan Pertandingan :</h1>
                      <div className="flex justify-around border rounded-xl p-2 mt-2">
                        <img src={cuplikan1} alt="" width={100} />
                        <img src={cuplikan2} alt="" width={100} />
                        <img src={cuplikan3} alt="" width={100} />
                      </div>
                    </div>
                    <div className="px-5 my-5 w-full">
                      <h1>Live Chat</h1>
                      <div className="text-xs font-mono">
                        <p className="py-1">Hebat banget!</p>
                        <p className="py-1">Hebat banget!</p>
                        <p className="py-1">Hebat banget!</p>
                        <p className="py-1">Hebat banget!</p>
                      </div>
                      <div className="flex justify-between items-center w-full">
                        <div className="w-full mr-3">
                          <input type="livechat" name="livechat" id="livechat" value="" className="border w-full p-1 rounded-lg" placeholder="Pesan..." />
                        </div>
                        <div>
                          <button type="button" className="bg-slate-400 px-3 py-1 rounded-lg text-white shadow-md">
                            Kirim
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="w-full md:w-1/4 border">
                <Berita />
              </div>
            </div>
          </div>
        </div>
      </div>
      <Footer />
    </div>
  );
};

export default Live;
