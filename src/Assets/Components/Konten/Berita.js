import React from "react";
import Berita1 from "../../Images/cth-berita.jpeg";
import Berita2 from "../../Images/cth2-berita.jpeg";

const Berita = () => {
  return (
    <div>
      <h1 className="text-lg font-bold text-center py-2">Berita</h1>
      <hr />
      <ul className="p-2">
        <li className="relative mt-3">
          <a href="" className="">
            <img src={Berita1} alt="" className="relative" />
            <div className="absolute bottom-0 text-white z-50 m-3">Contoh Judul Berita</div>
          </a>
        </li>
        <li className="relative mt-3">
          <a href="" className="">
            <img src={Berita2} alt="" className="relative" />
            <div className="absolute bottom-0 text-white z-50 m-3">Contoh Judul Berita 2</div>
          </a>
        </li>
      </ul>
    </div>
  );
};

export default Berita;
