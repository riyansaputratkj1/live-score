import React from "react";
import LogoPildun from "../../Images/wc-logo.png";
import LogoPL from "../../Images/pl-logo.png";
import LogoLL from "../../Images/ll-logo.png";
import { Link } from "react-router-dom";

const Kategori = () => {
  return (
    <div>
      <h1 className="text-lg font-bold text-center py-2">Kategori</h1>
      <hr />
      <div className="w-full box-border mt-3">
        <input type="text" name="pencarian" placeholder=" Cari Pertandingan..." className="border h-10 w-[215px] mx-3 rounded-lg" />
      </div>
      <div className="mt-4">
        <div className="flex border p-1">
          <Link to="/live">
            <a href="" className="flex items-center text-sm">
              <img src={LogoPildun} alt="" width={50} />
              <p>Piala Dunia 2022</p>
            </a>
          </Link>
        </div>
        <div className="flex border p-1">
          <a href="" className="flex items-center text-sm">
            <img src={LogoPL} alt="" width={50} />
            <p>Premier League</p>
          </a>
        </div>
        <div className="flex border p-1">
          <a href="" className="flex items-center text-sm">
            <img src={LogoLL} alt="" width={50} />
            <p>LaLiga</p>
          </a>
        </div>
      </div>
    </div>
  );
};

export default Kategori;
