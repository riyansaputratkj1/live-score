import React from "react";
import LogoPildun from "../../Images/wc-logo.png";
import LogoPL from "../../Images/pl-logo.png";
import LogoARG from "../../Images/arg-logo.png";
import LogoUAE from "../../Images/uea-logo.png";
import LogoPOR from "../../Images/por-logo.png";
import LogoBRZ from "../../Images/brz-logo.png";
import LogoMCT from "../../Images/mct-logo.png";
import LogoLVP from "../../Images/lvp-logo.png";
import LogoCHE from "../../Images/che-logo.png";
import LogoMCU from "../../Images/mcu-logo.png";
import { Link } from "react-router-dom";

const Pertandingan = () => {
  return (
    <div className="border">
      <h1 className="text-lg font-bold text-center py-2">Pertandingan</h1>
      <hr />
      <div>
        <ul className="mb-3">
          <div className="flex mt-3 border mb-3">
            <img src={LogoPildun} alt="" width={50} className="scale-75" />
            <h1 className="p-2">Piala Dunia 2022</h1>
          </div>
          <Link to="/live">
            <li className="mx-3 p-1 grid grid-cols-6 border items-center">
              <p className="px-4 text-sm">22:00</p>
              <div className="border-l w-full px-2 col-span-5">
                <a href="" className="grid items-center grid-cols-7 w-full">
                  <img src={LogoUAE} alt="" width={20} className="" />
                  <p className="text-sm col-span-5">Arab</p>
                  <p className="text-sm">0</p>
                </a>
                <a href="" className="grid items-center grid-cols-7 w-full">
                  <img src={LogoARG} alt="" width={20} className="mr-5" />
                  <p className="text-sm col-span-5">Argentina</p>
                  <p className="text-sm">0</p>
                </a>
              </div>
            </li>
          </Link>
          <li className="mx-3 p-1 grid grid-cols-6 border items-center">
            <p className="px-4 text-sm">13:00</p>
            <div className="border-l w-full px-2 col-span-5">
              <a href="" className="grid items-center grid-cols-7 w-full">
                <img src={LogoPOR} alt="" width={20} className="" />
                <p className="text-sm col-span-5">Portugal</p>
                <p className="text-sm">0</p>
              </a>
              <a href="" className="grid items-center grid-cols-7 w-full">
                <img src={LogoBRZ} alt="" width={20} className="mr-5" />
                <p className="text-sm col-span-5">Brazil</p>
                <p className="text-sm">0</p>
              </a>
            </div>
          </li>
        </ul>
        <ul className="mb-3">
          <div className="flex mt-3 border mb-3">
            <img src={LogoPL} alt="" width={50} className="scale-75" />
            <h1 className="p-2">Premiere League</h1>
          </div>
          <li className="mx-3 p-1 grid grid-cols-6 border items-center">
            <p className="px-4 text-sm">10:00</p>
            <div className="border-l w-full px-2 col-span-5">
              <a href="" className="grid items-center grid-cols-7 w-full">
                <img src={LogoLVP} alt="" width={20} className="" />
                <p className="text-sm col-span-5">Liverpool</p>
                <p className="text-sm">0</p>
              </a>
              <a href="" className="grid items-center grid-cols-7 w-full">
                <img src={LogoMCT} alt="" width={20} className="mr-5" />
                <p className="text-sm col-span-5">Mancester City</p>
                <p className="text-sm">0</p>
              </a>
            </div>
          </li>
          <li className="mx-3 p-1 grid grid-cols-6 border items-center">
            <p className="px-4 text-sm">06:00</p>
            <div className="border-l w-full px-2 col-span-5">
              <a href="" className="grid items-center grid-cols-7 w-full">
                <img src={LogoCHE} alt="" width={20} className="" />
                <p className="text-sm col-span-5">Chelsea</p>
                <p className="text-sm">0</p>
              </a>
              <a href="" className="grid items-center grid-cols-7 w-full">
                <img src={LogoMCU} alt="" width={20} className="mr-5" />
                <p className="text-sm col-span-5">Mancester United</p>
                <p className="text-sm">0</p>
              </a>
            </div>
          </li>
        </ul>
      </div>
    </div>
  );
};

export default Pertandingan;
