import React from "react";
import HeaderLogo from "../../Images/header.png";

const Header = () => {
  return (
    <div className="flex flex-wrap items-center">
      <div className="w-full md:w-1/2">
        <h3 className="text-base font-mono">Pantau Score Pertandinganmu,</h3>
        <h1 className="uppercase text-4xl font-bold">Bersama Live Score</h1>
        <p className="text-sm font-mono">Dimanapun dan Kapanpun...</p>
      </div>
      <div className="w-full md:w-1/2">
        <img src={HeaderLogo} alt="" width={300} />
      </div>
    </div>
  );
};

export default Header;
