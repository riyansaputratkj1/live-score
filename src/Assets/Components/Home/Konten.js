import React from "react";
import Berita from "../Konten/Berita";
import Kategori from "../Konten/Kategori";
import Pertandingan from "../Konten/Pertandingan";

const Konten = () => {
  return (
    <div className="mt-16">
      <div className="flex flex-wrap justify-between">
        <div className="w-full md:w-1/4 border">
          <Kategori />
        </div>
        <div className="w-full md:w-1/2 px-5">
          <Pertandingan />
        </div>
        <div className="w-full md:w-1/4 border">
          <Berita />
        </div>
      </div>
    </div>
  );
};

export default Konten;
