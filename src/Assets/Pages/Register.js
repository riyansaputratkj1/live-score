import React, { useState } from "react";
import { Link } from "react-router-dom";
import Logo from "../Images/logo.png";

const Register = () => {
  return (
    <div className="px-60">
      <div className="container flex justify-center">
        <div className="border rounded-xl shadow-2xl items-center mt-36 py-10 px-10">
          <div className="flex justify-center">
            <img src={Logo} alt="" width={120} />
          </div>
          <h1 className="text-center text-2xl font-bold">Sign Up</h1>
          <p className="text-xs font-mono text-center mb-5">masukan data untuk registrasi</p>
          <form className="">
            <div className="grid grid-cols-3 items-center mt-2 w-full">
              <label for="nama" className="">
                Name
              </label>
              <input type="text" name="nama" id="nama" placeholder=" Masukan name..." className="col-span-2 border w-[300px] p-2 rounded-lg" />
            </div>
            <div className="grid grid-cols-3 items-center w-full mt-2">
              <label for="email" className="">
                Email
              </label>
              <input type="email" name="email" id="email" placeholder=" Masukan email..." className="border w-[300px] p-2 rounded-lg col-span-2" />
            </div>
            <div className="grid grid-cols-3 items-center mt-2">
              <label for="password" className="">
                Password
              </label>
              <input type="password" name="password" id="password" placeholder=" Masukan password..." className="col-span-2 border w-full p-2 rounded-lg" />
            </div>
            <button type="submit" className="w-full py-2 px-7 mt-4 rounded-xl text-white shadow-lg borde bg-teal-500 hover:bg-white hover:text-teal-500 hover:shadow-none hover:border hover:border-teal-500">
              Sign up
            </button>
            <p className="text-xs font-mono cursor-pointer text-center mt-5">
              <Link to="/login">
                Sudah punya akun? <span className="text-teal-500"> Login.</span>
              </Link>
            </p>
          </form>
        </div>
      </div>
    </div>
  );
};

export default Register;
