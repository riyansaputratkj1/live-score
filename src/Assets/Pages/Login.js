import React, { useState } from "react";
import { Link } from "react-router-dom";
import Logo from "../Images/logo.png";

const Login = () => {
  return (
    <div className="px-60">
      <div className="container flex justify-center">
        <div className="border rounded-xl shadow-2xl items-center mt-36 p-10">
          <div className="flex justify-center">
            <img src={Logo} alt="" width={120} />
          </div>
          <h1 className="text-xl font-bold text-center">Selamat Datang,</h1>
          <p className="text-xs font-mono text-center mb-5">silahkan masukan data login dahulu.</p>
          <div className="grid grid-cols-3 items-center w-full">
            <label for="email" className="">
              Email
            </label>
            <input type="email" name="email" id="email" placeholder=" Masukan email..." className="border w-[300px] p-2 rounded-lg col-span-2" />
          </div>
          <div className="grid grid-cols-3 items-center mt-2">
            <label for="password" className="">
              Password
            </label>
            <input type="password" name="password" id="password" placeholder=" Masukan password..." className="col-span-2 border w-[300px] p-2 rounded-lg" />
          </div>
          <div className="flex flex-wrap justify-evenly mt-10">
            <Link to="/">
              <button className="py-2 px-7 rounded-full bg-teal-500 text-white shadow-lg hover:bg-white hover:text-teal-500 hover:border hover:border-teal-500 hover:shadow-none">Sign in</button>
            </Link>
            <Link to="/register">
              <button className="py-2 px-7 rounded-full text-teal-500 shadow-lg border border-teal-500 hover:bg-teal-500 hover:text-white">Sign up</button>
            </Link>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Login;
