import React from "react";
import "../../index.css";
import Header from "../Components/Home/Header";
import Konten from "../Components/Home/Konten";

const Home = () => {
  return (
    <div className="px-60">
      <div className="container">
        <Header />
        <Konten />
      </div>
    </div>
  );
};

export default Home;
