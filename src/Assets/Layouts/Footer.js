import React from "react";
import LogoLS from "../Images/logo.png";

const Footer = () => {
  return (
    <div className="px-60 mt-5">
      <div className="container border">
        <div className="grid grid-cols-6 w-full items-center">
          <div className="w-full">
            <img src={LogoLS} alt="" width={200} />
          </div>
          <div className="col-span-5 w-full">
            <p className="w-full text-sm text-center">
              Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a
              type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
            </p>
          </div>
        </div>
        <p className="text-center text-xs font-mono">Copyright@ 2022 | Live Score</p>
      </div>
    </div>
  );
};

export default Footer;
