import React from "react";
import { Link } from "react-router-dom";
import Logo from "../Images/logo.png";

const Navbar = () => {
  return (
    <header className="w-full px-60">
      <div className="flex container mx-auto justify-between items-center ">
        <img src={Logo} alt="" className="" width={100} />
        <div>
          <nav className="w-full">
            <ul className="flex border rounded-l-full bg-sky-900">
              <li className="px-5 py-3 ml-0 text-lg text-white hover:border-1 hover:text-slate-800 hover:rounded-l-full cursor-pointer hover:bg-slate-200">
                <Link to="/">Home</Link>
              </li>
              <li className="px-8 py-3 text-lg text-white hover:border-1 hover:text-slate-800 cursor-pointer hover:bg-slate-200">Live</li>
              <li className="px-5 py-3 text-lg text-white hover:border-1 hover:text-slate-800 cursor-pointer hover:bg-slate-200">Berita</li>
              <li className="px-5 py-3 text-lg text-white hover:border-1 hover:text-slate-800 cursor-pointer hover:bg-slate-200">
                <Link to="/login">Login</Link>
              </li>
            </ul>
          </nav>
        </div>
      </div>
    </header>
  );
};

export default Navbar;
